#!/bin/bash
#SBATCH -J lmp
#SBATCH -p normal
#SBATCH -N 1 
#SBATCH --ntasks-per-node=4
#SBATCH --exclusive
#SBATCH --gres=dcu:4
#SBATCH -o %x-%j.log 

#tar xzvf lammps-2Aug2023.tar.gz
export LAMMPS_ROOT=$HOME/local/lammps-2Aug2023
export LMP_PATH=${LAMMPS_ROOT}/bin/lmp_hip
export LD_LIBRARY_PATH=${LAMMPS_ROOT}/lib64:${LD_LIBRARY_PATH}
export NAME_PREFIX=${SLURM_JOB_NAME}-${SLURM_JOBID}-${SLURM_NTASKS}n${SLURM_NNODES}N
export OMP_NUM_THREADS=6

module purge                    
module load compiler/devtoolset/7.3.1
module load mpi/hpcx/2.12.0/gcc-7.3.1
module load compiler/rocm/dtk/23.10.1
module load compiler/cmake/3.24.1
module load mathlib/fftw/3.3.8/double/gnu
module load mathlib/openblas/gnu/0.3.7
module load mathlib/gsl/2.7/gnu


env|grep SLURM
srun hostname |sort|awk 'NF==1{arr[$1]++} END{for (i in arr){printf "%s:%d\n",i,arr[i]}}'

mpirun -n ${SLURM_NTASKS} ${LMP_PATH} -h

#GPU mode
#mpirun -n ${SLURM_NTASKS} ${LMP_PATH} -nocite -sf gpu -pk gpu 4 -in in.lj -log ${NAME_PREFIX}-hip.log 
#CPU mode
#mpirun -n ${SLURM_NTASKS} ${LMP_PATH} -nocite -in in.lj -log ${NAME_PREFIX}-cpu.log 

#sbatch this.sh
