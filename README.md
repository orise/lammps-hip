# 东方超算系统LAMMPS使用指南

文档版本v1.0，2023年4月26日

文档版本v1.1，2024年5月13日

徐顺， xushun@sccas.cn



本指南主要面向基于C86平台的**东方超算系统**用户，也可作为其他平台用户参考。C86平台的CPU处理器兼容X86指令集，类GPU处理器DCU采用兼容AMD ROCM的DTK软件开发与运行环境。

# 一、概要

LAMMPS软件名由英文Large-scale Atomic/Molecular Massively Parallel Simulator缩写而来，该软件的开发起源于美国能源部DOE的Sandia国家实验室，是由DOE支持并遵循 GNU Public License (GPL)版权的开源软件。LAMMPS是一个经典分子动力学模拟软件，其模拟粒子之间作用力服从牛顿运动定律，一般包含短程和长程作用力。建模的粒子对象可以是原子的、多聚物的、生物体的、颗粒物的、粗粒化、介观的和宏观的作用力粒子，模拟物质可以是2D或3D维度的多边界条件的气态、液体和固态形式。LAMMPS软件中模拟的粒子、力场、约束具有很强的扩展性，代码开发活跃，全球存在很多使用者和贡献者。

LAMMPS软件以MPI为主要并行计算框架，可组合共享内存多线程和异构计算加速模式，在桌面机和大规模并行机上都可以运行；采用空间划分并行方式（spatial-decomposition）将整个模拟区域上的计算任务按子区域划分，子区域上的计算任务分配给各处理器并行处理，每个处理器除了计算其负责的子区域内部粒子计算，还要为其子区域边界区的粒子存储和交换“幽灵”（ghost）粒子信息，模拟粒子数可以到达10万或10亿量级。程序由早期的F77和F90代码改为现在的C++形式，但C++的面向对象特征仅仅使用了常用的继承和多态性形式，没有非常繁杂的C++特征，主要便于软件新功能的扩展，包含力场、原子类型和边界条件等。

- 官方网站: https://www.lammps.org

- 代码库: https://github.com/lammps/lammps

- 使用手册  https://docs.lammps.org/Manual.html

- 编译说明  https://docs.lammps.org/Build.html     

- 开发指南  https://docs.lammps.org/Developer.html    


# 二、源代码说明

LAMMPS软件会发布一些稳定性的源代码版本，下载地址https://www.lammps.org/download.html, 也可以去[github库]( https://github.com/lammps/lammps)下载全部历史代码。下载并解压最新版本的源代码包

```bash
wget https://download.lammps.org/tars/lammps.tar.gz
tar xzvf lammps.tar.gz
```

稳定版下载

```
wget https://download.lammps.org/tars/lammps-stable.tar.gz
```

LAMMPS发行的源代码中包含以下目录和文件：

- [src](https://github.com/lammps/lammps/tree/develop/src)  源代码程序文件，进入该目录进行make安装
- [lib](https://github.com/lammps/lammps/tree/develop/doc) 主程序依赖的扩展模块库，如gpu、plumed等模块
- [python](https://github.com/lammps/lammps/tree/develop/python)  主程序的Python模块API包装
- [potentials](https://github.com/lammps/lammps/tree/develop/potentials) 原子作用势文件
- [cmake](https://github.com/lammps/lammps/tree/develop/cmake)  CMake编译相关文件
- [tools](https://github.com/lammps/lammps/tree/develop/tools)  预处理和后处理工具
- [doc](https://github.com/lammps/lammps/tree/develop/doc) 使用文档，在其中可以使用`make html`或`make pdf`命令生成离线文档
- [examples](https://github.com/lammps/lammps/tree/develop/examples) 各类测试实例
- [bench](https://github.com/lammps/lammps/tree/develop/bench)  基准测试集

LAMMPS版本号采用发行日期作为版本号，如18 May 2018，版本号在源代码src/version.h中定义，会在运行输出的screen和logfile文件中找到。

# 三、软件安装

LAMMPS软件功能模块的定制性很强，用户可以根据需要安装指定的模块。在东方超算系统中，已提供编译的LAMMPS版本，可执行文件为lmp_hip，使用以下命令加载：

```
module load apps/lammps-DCU/2Aug2023/hpcx-2.7.4-dtk23.10
```

一般用户可跳过LAMMPS“软件安装”这节，直接转到“实例测试”。

如用户用到某些扩展模块没有在已编译的LAMMPS版本中，这时用户就需要学会自己编译LAMMPS软件。

LAMMPS编译方式具有高可配置性：

- 提供make和cmake两个基本的编译方式
- 可用MPI编译并行版本(make mpi)，也提供串行单机版本(make serial)
- 通过package来封装和扩展功能模块，提供编译开关
- 提供GPU、OPENMP、KOKKOS、Intel MIC等可选加速计算模块
- 支持动态链接库的编译方式，也支持Python模块的编译方式

编译LAMMPS之前我们需要确认C++编译器和Fortran编译器是否可用，相关的MPI编译位置等信息。基本的编译器为C++，如果编译LAMMPS的MPI版本需要mpicxx，如果要编译一些可选库也需要fortran编译器。依赖FFT动态库，一般使用[fftw3](http://fftw.org/download.html)库（编译时configure命令加上--enable-shared选项），也可以使用MKL中提供的FFT库；最好提供blas和lapack数值计算库，否者使用自带的数值运算库，计算效率会变低；如果要输出构型图像，也需要jpge和png等库；如果需要GPU加速则需要CUDA SDK或者OpenCL运行库，DCU加速需要ROCm/HIP环境。LAMMPS各个功能模块称为package， 编译时可选择编译开关，建议先定位建模的功能指令所属的package，在编译时保证该package已开启编译。 LAMMPS提供了安装帮助文档：https://docs.lammps.org/Install.html。

这里选择如下GCC、MPI和ROCm/DTK编译环境：

```bash
module purge                                                                    
module load compiler/devtoolset/7.3.1                                           
module load mpi/hpcx/2.12.0/gcc-7.3.1                                            
module load compiler/rocm/dtk/23.10.1                                                
```

这里安装most package集，它包含大多数不需要依赖外部的库，通过cmake/preset/most.cmake可以查看most集合含以下package：

```
AMOEBA  ASPHERE  BOCS  BODY  BPM  BROWNIAN  CG-DNA  CG-SPICA  CLASS2  COLLOID  COLVARS  COMPRESS  CORESHELL  DIELECTRIC  DIFFRACTION  DIPOLE  DPD-BASIC  DPD-MESO  DPD-REACT  DPD-SMOOTH  DRUDE  ELECTRODE  EFF  EXTRA-COMPUTE  EXTRA-DUMP  EXTRA-FIX  EXTRA-MOLECULE  EXTRA-PAIR  FEP  GRANULAR  INTERLAYER  KSPACE  LEPTON  MACHDYN  MANYBODY  MC  MEAM  MESONT  MISC  ML-IAP  ML-POD  ML-SNAP  MOFFF  MOLECULE  OPENMP  OPT  ORIENT  PERI  PHONON  PLUGIN  POEMS  QEQ  REACTION  REAXFF  REPLICA  RIGID  SHOCK  SPH  SPIN  SRD  TALLY  UEF  VORONOI  YAFF
```

注意在most集合包中，针对计算加速方面，包含openmp package，但不包含gpu package；针对集体变量计算，包含colvars package，但不包含plumed package。**还需要注意**src/Makefile定义PACKMOST变量中并不包含colvars，PACKLIB中才包括colvars；也就是说Make编译方式和CMake编译方式定义的most集合包并不完全相同，这是一个LAMMPS开发时未同步的问题。

## Make安装方式

LAMMPS传统编译方式采用make方式。即进入src目录，调用各个预定义的Makefile.xxx进行编译。它可以灵活定制各功能模块，一般在UNIX、Linux和MAC OSX系统中采用。

LAMMPS提供mpi和serial两个默认的make target，分别代表编译MPI 并行版本和单机串行版，成为lmp_mpi和lmp_serial，默认在src目录，可以拷贝到某个PATH路径下。我们也可以选择其他make target，参考src/MAKE/MACHINES和src/MAKE/OPTIONS目录中提供的Makefile.xxx模板进行修改，配置适合编译系统的参数，然后在src目录使用make xxx来调用这个编译配置，生成lmp_xxx可执行文件。比如Makefile.ubuntu就是为Ubuntu Linux提供的默认参数配置，其中配置环境使用了g++、mpi-default、libfftw3-dev和libjpeg-dev等，在src目录使用make ubuntu就可以编译生成lmp_ubuntu。同样在src目录使用make hip就可以编译生成lmp_hip。

以安装most集合包、colvars和plumed独立包为例，命令行为

```bash
cd src
#make no-all
make yes-most yes-colvars yes-plumed
```

注意colvars package依赖lepton package。其中most集合包含了mesont package，它会去联网下载`C_10_10.mesocnt`保存在`potentials/C_10_10.mesocnt`路径。plumed pacakge安装也会联网下载相应的PLUMED2软件包来编译。因此以上过程需要服务器需要连接外网。在东方超算系统默认关闭HTTP外网访问，推荐通过SSH转发HTTP代理端口来连外网，参考集群[无法访问外网HTTP服务](https://gitlab.com/xushun/linux/shell/-/blob/master/cluster_faq.md#2%E6%97%A0%E6%B3%95%E8%AE%BF%E9%97%AE%E5%A4%96%E7%BD%91http%E6%9C%8D%E5%8A%A1)的FAQ文档进行解决。

可用使用命令查看各个package是否已选择安装：

```
make ps #status of all packages
make pi #list of installed packages
```
在可以src目录调用make命令编译各个package对应lib，一般会编译成对应的静态库文件，以供LAMMPS主程序链接。
```bash
#cd src
make lib-plumed args="-b"
make lib-colvars args="-m mpi"
```

以上命令完成plumed和colvars两个package的（下载）编译。

接下来调用一个machine，进行编译，这个machine对应MAKE目录中定义的Makefile.machine。如选择machine为mpi开始编译：

```
make mpi -j6
```

这里使用6个处理器核并行编译，结果会生成`lmp_mpi`可执行文件。

传统的Make方式未定义`make install`，需手动拷贝安装，可以使用如下脚本：

```bash
export LMP_DIR=$HOME/local/lammps-2Aug2023
mkdir -p $LMP_DIR/bin
cp lmp_mpi $LMP_DIR/bin
mkdir -p $LMP_DIR/share/lammps
cp -r ../potentials $LMP_DIR/share/lammps
```

其中`$LMP_DIR`为LAMMPS安装目录。

## CMake安装方式

LAMMPS除了提供传统的进入src执行make编译操作，还提供cmake方式。cmake方式克服了适配不同环境，手动配置编译参数的麻烦。CMake编译方式：进入LAMMPS源代码根目录，新建临时编译目录，并执行相应的camke命令

```bash
#module load compiler/cmake/3.24.1
mkdir build
cd build
cmake -L -DCMAKE_INSTALL_PREFIX=$HOME/local/lammps-2Aug2023 -C ../cmake/presets/most.cmake \
 -DPKG_COLVARS=ON -DPKG_PLUMED=ON -DBUILD_TOOLS=ON -DLAMMPS_MACHINE=mpi ../cmake
cmake --build . --parallel 6
cmake --install .
```

通过cmake命令可以给定安装目录，通过preset选择预定义package集，通过`-DPKG_XXX`控制XXX package是否编译，通过`-DLAMMPS_MACHINE`可以指定LAMMPS编译后可执行文件的后缀名，如`-DLAMMPS_MACHINE=mpi`则编译之后获得`lmp_mpi`可执行文件名。这个后缀名在cmake编译模式下可以任意指定，这与LAMMPS传统make方式不同，传统方式中后缀xxx需要在MAKE目录中有对应的Makefile.xxx文件存在。

## 编译HIP版本

**编译GPU package**

LAMMPS起初提供GPU和USER-CUDA两个package，它们是两种不同的GPU实现，后来LAMMPS官方代码中只保留了[GPU package](https://lammps.sandia.gov/doc/Packages_details.html#pkg-gpu)以支持GPU加速，而GPU package早期是基于CUDA设计的，后期增加了OpenCL模块，实现了在Geryon框架下的统一库，但支持OpenCL功能开发并不活跃。2021年左右，GPU package增加了ROCm HIP的GPU计算加速。在C86平台可以使用OpenCL加速，也可以使用HIP加速方式。如果基于OpenCL的LAMMPS编译，可以使用CUDA SDK中自带的[OpenCL](https://developer.nvidia.com/opencl)，也可以使用AMD ROCm SDK中提供的OpenCL。在**C86平台推荐使用GPU package的HIP加速方式**，比OpenCL加速方式大约有3%的速度提升。

编译GPU版本，先要编译lib/gpu库，它是在Geryon框架下设计的，包括CUDA、OpenCL、HIP三种编译方式，都会生成动态库供LAMMPS主程序调用。编译hip版本，需要先加载MPI和HIP环境

```bash
module purge                                                                    
module load compiler/devtoolset/7.3.1                                           
module load mpi/hpcx/2.12.0/gcc-7.3.1                                            
module load compiler/rocm/dtk/23.10.1   
```

**a. 使用Make编译时**，在src目录选择gpu package并进行hip方式的编译

```bash
cd src
make yes-most yes-colvars yes-plumed yes-gpu
make lib-plumed args="-b"
make lib-colvars args="-m mpi"
make lib-gpu args="-m hip -b"
```

应该most集合包含了openmp包（相当于yes-openmp），则需要在src/MAKE/OPTIONS/Makefile.hip的LINKFLAGS中添加-fopenmp选项，如

```Makefile
HIP_PATH ?= $(wildcard /opt/rocm/hip)
LINK =          $(HIP_PATH)/bin/hipcc
LINKFLAGS =     -g -O3 -fopenmp $(shell mpicxx --showme:link)
```
采用6个处理器核并行编译，使用`VERBOSE=1`使得编译同时显示命令行，方便调试编译错误。
```
make hip -j6 VERBOSE=1
```
编译成功后在src目录生成可执行文件lmp_hip，这就是LAMMPS主程序。

**b. 使用cmake编译时**，需设置`-DPKG_GPU=ON`和`-DGPU_API=hip`选项。LAMMPS源代码针对gpu package定义了变量GPU_API（见`cmake/Modules/Packages/GPU.cmake`），改变量可取opencl、cuda、hip三个值，用于控制gpu package采用哪种GPU加速运行库。在C86平台上，支持opencl和hip两种运行库来编译gpu package。首先进入源代码根目录，对于hip编译方式为：

```bash
#module load compiler/cmake/3.24.1
mkdir build
cd build
cmake -L -DCMAKE_INSTALL_PREFIX=$HOME/local/lammps-2Aug2023 -DBUILD_TOOLS=ON \
-C ../cmake/presets/most.cmake -DPKG_COLVARS=ON -DPKG_PLUMED=ON \
-DPKG_GPU=ON -DGPU_API=hip -DCMAKE_CXX_COMPILER=hipcc -DLAMMPS_MACHINE=hip ../cmake
make -j 6 VERBOSE=1
make install
```

这里需要注意当`-DGPU_API=hip`时，需要设置`-DCMAKE_CXX_COMPILER=hipcc`，否则使用普通的C++编译器无法编译gpu package中的hip代码。`-DCMAKE_CXX_COMPILER=hipcc`这个选项可以用`-C ../cmake/presets/hip_amd.cmake`或者`-C ../cmake/presets/hip.cmake`代替，presets目录中的这两个cmake文件中已定义了`CMAKE_CXX_COMPILER`变量为`hipcc`。

**编译GPU、Kokkos package**

有些力场如ReaxFF并没有GPU package实现GPU加速，而是通过Kokkos方式提供GPU加速。

东方超算中可确认预编译的LAMMPS软件环境是否支持相关力场的Kokkos加速。

```
module add apps/lammps-DCU/2Aug2023/hpcx-2.7.4-dtk23.10
```

在计算节点上查看LAMMPS加速计算模式，通过lmp_mpi -h输出日志

```
srun -p normal -n 1 --gres=dcu:1 mpirun -n 1 /public/software/apps/lammps-DCU/lammps_2Aug2023/lammps-install/bin/lmp_mpi -h
```

确认如下：

```
Accelerator configuration:

GPU package API: HIP
GPU package precision: mixed
KOKKOS package API: HIP OpenMP Serial
KOKKOS package precision: double
OPENMP package API: OpenMP
OPENMP package precision: double

Compatible GPU present: yes
```

进一步，查看ReaxFF力场加速的方式

```
$ lmp_mpi -h|grep reaxff
polymorphic     reaxff          reax/c          reaxff/kk       reax/c/kk
reaxff/omp      reax/c/omp      rebo            rebo/omp        resquared
accelerate/cos  acks2/reax      acks2/reaxff    acks2/reaxff/kk acks2/reax/kk
qeq/reaxff      qeq/reax        qeq/reaxff/kk   qeq/reax/kk     qeq/reaxff/omp
qeq/reax/omp    qeq/shielded    qeq/slater      rattle          reaxff/bonds
reax/c/bonds    reaxff/bonds/kk reax/c/bonds/kk reaxff/species  reax/c/species
reaxff/species/kk               reax/c/species/kk               recenter
```

可见ReaxFF力场常用的reaxff和qeq/reaxff只提供KOKKOS加速版，没有GPU加速版。实际确认LAMMPS GPU package没有对ReaxFF计算的加速实现。

我们使用CMake编译GPU package、Kokkos pacakge、PLUMED package和Colvars pacakge，使用CMake编译相对直接makefile方式配置简单，适合编译模块较多的时候。

加载编译环境

```
module purge                                                                    
module load compiler/devtoolset/7.3.1                                           
module load mpi/hpcx/2.12.0/gcc-7.3.1                                            
module load compiler/rocm/dtk/23.10.1   
module load compiler/cmake/3.24.1
module load mathlib/fftw/3.3.8/double/gnu
module load mathlib/openblas/gnu/0.3.7
module load mathlib/gsl/2.7/gnu
```

这里gls和openblas为PLUMED package依赖库。PLUMED会自动下载，这里需要配置http代理，如

```
export http_proxy=http://127.0.0.1:9080
export https_proxy=http://127.0.0.1:9080
```

cmake编译

```bash
cd lammps-2Aug2023
mkdir build && cd build
cmake -L -C ../cmake/presets/most.cmake -C ../cmake/presets/hip_amd.cmake -C ../cmake/presets/kokkos-hip.cmake \
-DCMAKE_INSTALL_PREFIX=$HOME/local/lammps-2Aug2023 -DBUILD_TOOLS=ON -DBUILD_SHARED_LIBS=ON \
-DPKG_COLVARS=ON -DPKG_PLUMED=ON -DPKG_GPU=ON -DGPU_API=hip -DHIP_ARCH=gfx906 \
-DFFT=FFTW3 -DFFTW3_LIBRARY=/public/software/mathlib/fftw/3.3.8/double/gnu/lib/libfftw3.so \
-DFFTW3_INCLUDE_DIR=/public/software/mathlib/fftw/3.3.8/double/gnu/include \
-DBUILD_MPI=ON -DLAMMPS_MACHINE=mpi ../cmake
```

BUILD_SHARED_LIBS默认为OFF，也可以设为ON，这样在安装目录生成include和lib64目录。most集合包含了reaxff、kspace和extra-compute包。而cmake/presets/kokkos-hip.cmake中定义了PKG_KOKKOS=ON，且会打开HIP、SERIAL和OPENMP后端支持，并会关闭CUDA后端支持。将kokkos-hip.cmake中Kokkos_ARCH_VEGA90A改为Kokkos_ARCH_VEGA906，如

```
set(Kokkos_ARCH_VEGA906 ON CACHE BOOL "" FORCE)
```

否则即使编译通过，在运行时Kokkos也会报错。如

```
terminate called after throwing an instance of 'std::runtime_error'
  what():  hipMemcpyToSymbol( HIP_SYMBOL(::Kokkos::Impl::g_device_hip_lock_arrays), &::Kokkos::Impl::g_host_hip_lock_arrays, sizeof(::Kokkos::Impl::HIPLockArrays)) error( hipErrorInvalidKernelFile): invalid kernel file /public/home/xushun/lammps_dev/lammps-2Aug2023/lib/kokkos/core/src/HIP/Kokkos_HIP_Locks.cpp:96
```
如果链接lmp_mpi报“cannot find -lomp”错误，修改CMakeFiles/lmp.dir/link.txt删除-lomp。

编译安装

```
make -j 12 VERBOSE=1
make install
```
编译完成后build大小3.4 GB。

在/public/software/compiler/dtk/dtk-23.04/hipfft/lib/cmake目录hipfft-config.cmake文件提供

```
set(HIPFFT_LIBRARY hip::hipfft)
```

的定义，指向/public/software/compiler/dtk/dtk-23.10.1/lib/libhipfft.so，如果需要fft.so，可以从CMake的编译选项中传入如

```
-DHIPFFT_LIBRARY=/public/software/compiler/dtk/dtk-23.10.1/lib/libhipfft.so
```


```
srun -p normal -n 1 --gres=dcu:1 mpirun -n 1 $HOME/local/lammps-2Aug2023/bin/lmp_hip -h
```

查看GPU、KOKKOS package编译状态

```
Accelerator configuration:

GPU package API: HIP
GPU package precision: mixed
KOKKOS package API: HIP OpenMP Serial
KOKKOS package precision: double
Kokkos library version: 3.7.2
OPENMP package API: OpenMP
OPENMP package precision: double
OpenMP standard: OpenMP 5.0

Compatible GPU present: yes

Active compile time flags:

-DLAMMPS_GZIP
-DLAMMPS_PNG
-DLAMMPS_JPEG
-DLAMMPS_SMALLBIG
sizeof(smallint): 32-bit
sizeof(imageint): 32-bit
sizeof(tagint):   32-bit
sizeof(bigint):   64-bit
```

而且可以看到kk、openmp和gpu等后缀力场类型。

新建module文件

```tcl
#%Module1.0

module unload compiler/rocm
module unload mpi/hpcx

module unload mathlib/fftw
module unload mathlib/openblas
module unload mathlib/gsl

module load compiler/devtoolset/7.3.1                                           
module load mpi/hpcx/2.12.0/gcc-7.3.1                                            
module load compiler/rocm/dtk/23.10.1

module load mathlib/fftw/3.3.8/double/gnu
module load mathlib/openblas/gnu/0.3.7
module load mathlib/gsl/2.7/gnu

#for Kokkos
setenv OMP_PROC_BIND spread 
setenv OMP_PLACES threads
setenv OMP_PROC_BIND true

set             APPS_HOME            $::env(HOME)/local/lammps-2Aug2023

prepend-path    PATH                    ${APPS_HOME}
prepend-path    PATH                    ${APPS_HOME}/bin
prepend-path    LD_LIBRARY_PATH         ${APPS_HOME}/lib
prepend-path    LD_LIBRARY_PATH         ${APPS_HOME}/lib64
prepend-path    PKG_CONFIG_PATH         ${APPS_HOME}/lib64/pkgconfig
prepend-path    INCLUDE                 ${APPS_HOME}/include
prepend-path    MANPATH                 ${APPS_HOME}/share/man
```

其中OMP_xxx环境变量是Kokkos运行时需要的环境变量，不设置会提示设置。

## 编译Torch版

我们使用CMake编译GPU package、Kokkos pacakge、PLUMED package和Colvars pacakge，另外还将编译基于Pytorch的Allegro机器学习模型。

加载编译环境

```bash
module purge                                                                    
module load compiler/devtoolset/7.3.1                                           
module load mpi/hpcx/2.7.4/gcc-7.3.1

module load compiler/cmake/3.24.1
module load mathlib/fftw/3.3.8/double/gnu
module load mathlib/openblas/gnu/0.3.7
module load mathlib/gsl/2.7/gnu
#module load apps/PyTorch/2.1.0/pytorch-2.1.0-dtk2310

source /public/software/compiler/dtk/dtk-23.10/env.sh
PYTORCH_PATH=/public/software/apps/DeepLearning/PyTorch
PYTORCH_DTK_PATH=${PYTORCH_PATH}/pytorch-2.1.0-dtk2310
export PATH=${PYTORCH_DTK_PATH}/bin:${PATH}
export LD_LIBRARY_PATH=${PYTORCH_DTK_PATH}/lib:/public/software/apps/DeepLearning/Libs/openssl-1.1.0j/lib:${PYTORCH_PATH}/openblas-0.3.7-build/lib:${PYTORCH_PATH}/lmdb-0.9.24-build/lib:${PYTORCH_PATH}/opencv-2.4.13.6-build/lib:/public/software/compiler/dtk/dtk-23.10/miopen/lib:/public/software/mathlib/boost/1_72_0/lib:${LD_LIBRARY_PATH}
export MIOPEN_SYSTEM_DB_PATH=/temp/pytorch-miopen-2.8
export PYTHONPATH=${PYTORCH_DTK_PATH}:${PYTHONPATH}
```

这里gls和openblas为PLUMED package依赖库。PLUMED会自动下载，这里需要配置http代理，如

```
export http_proxy=http://127.0.0.1:9080
export https_proxy=http://127.0.0.1:9080
```

```
[xushun@login08 lammps-17Apr2024]$ which python
/public/software/apps/DeepLearning/PyTorch/pytorch-2.1.0-dtk2310/bin/python
[xushun@login08 lammps-17Apr2024]$ python
Python 3.8.18 | packaged by conda-forge | (default, Dec 23 2023, 17:21:28)
[GCC 12.3.0] on linux
Type "help", "copyright", "credits" or "license" for more information.
>>> import torch
>>> torch.compiled_with_cxx11_abi()
False
```

添加补丁

```
cd ~/lammps_dev/pair_allegro
./patch_lammps.sh ~/lammps_dev/lammps-17Apr2024
```

修改将kokkos-hip.cmake中Kokkos_ARCH_VEGA90A改为Kokkos_ARCH_VEGA906

cmake编译

```bash
cd lammps-17Apr2024
mkdir build && cd build
export TORCH_INC=/public/software/apps/DeepLearning/PyTorch/pytorch-2.1.0-dtk2310/lib/python3.8/site-packages/torch/include
export TORCH_LIB_DIR=/public/software/apps/DeepLearning/PyTorch/pytorch-2.1.0-dtk2310/lib/python3.8/site-packages/torch/lib

cmake -L -C ../cmake/presets/most.cmake -C ../cmake/presets/hip_amd.cmake \
-C ../cmake/presets/kokkos-hip.cmake -DCMAKE_CXX_COMPILER=hipcc \
-DTorch_DIR=`python -c 'import torch;print(torch.utils.cmake_prefix_path)'`/Torch \
-DGFLAGS_INCLUDE_DIR=$TORCH_INC \
-DGFLAGS_LIBRARY=$TORCH_LIB_DIR/libgflags.so \
-DGLOG_INCLUDE_DIR=$TORCH_INC \
-DGLOG_LIBRARY=$TORCH_LIB_DIR/libglog.so \
-DMKL_INCLUDE_DIR=/public/software/compiler/intel/2021.3.0/mkl/include \
-DCMAKE_INSTALL_PREFIX=$HOME/local/lammps-17Aug2024-torch -DBUILD_TOOLS=ON -DBUILD_SHARED_LIBS=ON \
-DPKG_COLVARS=ON -DPKG_PLUMED=ON -DPKG_GPU=ON -DGPU_API=hip -DHIP_ARCH=gfx906 \
-DFFT=FFTW3 -DFFTW3_LIBRARY=/public/software/mathlib/fftw/3.3.8/double/gnu/lib/libfftw3.so \
-DFFTW3_INCLUDE_DIR=/public/software/mathlib/fftw/3.3.8/double/gnu/include \
-DBUILD_MPI=ON -DLAMMPS_MACHINE=mpi ../cmake
```

之后开始编译

```
make -j20
make install
```

如果make编译过程出现找不到libamdhip64.so的错误

```
make[2]: *** No rule to make target '/opt/dtk/hip/lib/libamdhip64.so', needed by 'liblammps_mpi.so.0'.  Stop.
make[1]: *** [CMakeFiles/Makefile2:456: CMakeFiles/lammps.dir/all] Error 2
make: *** [Makefile:136: all] Error 2
```

则将CMakeFiles/lmp.dir/build.make和 CMakeFiles/lammps.dir/build.make中的路径

```
lmp_mpi: /opt/dtk/hip/lib/libamdhip64.so
lmp_mpi: /opt/dtk/roctracer/lib/libroctx64.so
```

修改为

```
lmp_mpi: /public/software/compiler/dtk/dtk-23.10/hip/lib/libamdhip64.so
lmp_mpi: /public/software/compiler/dtk/dtk-23.10/roctracer/lib/libroctx64.so
```
使用命令行操作为
```
sed -i -e "s|/opt/dtk/hip/lib/libamdhip64.so|/public/software/compiler/dtk/dtk-23.10/hip/lib/libamdhip64.so|g" \
    -e "s|/opt/dtk/roctracer/lib/libroctx64.so|/public/software/compiler/dtk/dtk-23.10/roctracer/lib/libroctx64.so|g" \
    CMakeFiles/lammps.dir/build.make CMakeFiles/lmp.dir/build.make
```

同时修改CMakeFiles/lammps.dir/link.txt和CMakeFiles/lmp.dir/link.txt，命令行操作为

```
sed -i -e "s|-lomp|-L/public/software/compiler/dtk/dtk-23.10/llvm/lib/ -lomp|g" \
    -e "s|-lroctx64|-L/public/software/compiler/dtk/dtk-23.10/roctracer/lib -lroctx64|g" \
    CMakeFiles/lammps.dir/link.txt CMakeFiles/lmp.dir/link.txt
```

**实例运行**

新建环境lmp_torch_env.sh

```bash
module purge                                                                    
module load compiler/devtoolset/7.3.1                                           
module load mpi/hpcx/2.7.4/gcc-7.3.1

module load mathlib/fftw/3.3.8/double/gnu
module load mathlib/openblas/gnu/0.3.7
module load mathlib/gsl/2.7/gnu
#module load apps/PyTorch/2.1.0/pytorch-2.1.0-dtk2310

source /public/software/compiler/dtk/dtk-23.10/env.sh
PYTORCH_PATH=/public/software/apps/DeepLearning/PyTorch
PYTORCH_DTK_PATH=${PYTORCH_PATH}/pytorch-2.1.0-dtk2310
TORCH_LIB_DIR=${PYTORCH_DTK_PATH}/lib/python3.8/site-packages/torch/lib

LMP_PATH=$HOME/local/lammps-17Aug2024-torch
export PATH=${PYTORCH_DTK_PATH}/bin:$LMP_PATH/bin:${PATH}
export LD_LIBRARY_PATH=${PYTORCH_DTK_PATH}/lib:$LMP_PATH/lib64:${TORCH_LIB_DIR}:/public/software/apps/DeepLearning/Libs/openssl-1.1.0j/lib:${PYTORCH_PATH}/openblas-0.3.7-build/lib:${PYTORCH_PATH}/lmdb-0.9.24-build/lib:${PYTORCH_PATH}/opencv-2.4.13.6-build/lib:/public/software/compiler/dtk/dtk-23.10/miopen/lib:/public/software/mathlib/boost/1_72_0/lib:${LD_LIBRARY_PATH}
export MIOPEN_SYSTEM_DB_PATH=/temp/pytorch-miopen-2.8
export PYTHONPATH=${PYTORCH_DTK_PATH}:${PYTHONPATH}
```

使用LAMMPS Torch版本，提交一个基于Allegro机器学习作用势模型的实例，加载lmp_torch_env.sh。

脚本名设为allegro.slurm

```bash
#!/bin/bash
#SBATCH -J allegro
#SBATCH -p normal
#SBATCH --ntasks-per-node=2
#SBATCH --cpus-per-task=4
#SBATCH --gres=dcu:2
#SBATCH -o %x-%j.log

#env|grep SLURM
echo "CUDA_VISIBLE_DEVICES = $CUDA_VISIBLE_DEVICES"

source lmp_torch_env.sh
export NAME_PREFIX=${SLURM_JOB_NAME}-${SLURM_JOBID}-${SLURM_NTASKS}n${SLURM_NNODES}N
export OMP_NUM_THREADS=4
export OMP_PROC_BIND=true

date
#mpirun -n ${SLURM_NTASKS} lmp_mpi -nocite -h | grep -i -E "Kokkos|allegro"
mpirun -n ${SLURM_NTASKS} lmp_mpi -nocite -sf kk -k on g 2 t ${OMP_NUM_THREADS} -pk kokkos newton on neigh full -log ${NAME_PREFIX}-kk.log -in input.lammps
```

## 编译Torch+TensorFlow版本

LAMMPS支持DeepMD和Allegro机器学习势，DeepMD依赖TernsorFlow深度学习框架，而Allegro依赖Pytorch深度学习框架，因此我们可在东方超算中，使用conda创建一个Python环境，并安装TernsorFlow和Pytorch框架，基于此编译LAMMPS的DeepMD和Allegro机器学习势。此编译过程较复杂，对编译好的版本可二进制打包共享给其他用户。

注意：东方超算上在公共目录`/public/software/apps/lammps-dl/py3dl`已经编译安装LAMPPS的Torch和TensorFlow版本，导入命令为

```
source /public/software/apps/lammps-dl/py3dl/lammps-17Aug2024/bin/lmp_env_dl.sh
```

以下为本地编译安装过程的说明，可以作为自定义安装的参考。

在DTK开发者社区下载合适版本的Torch和TensorFlow安装包，其中tensorflow基于DTK 23.xx需要glibc 2.9高版本，由于东方平台glibc是glibc 2.5，这里选择基于22.10版本tensorflow版本

```bash
wget https://cancon.hpccube.com:65024/directlink/4/pytorch/previous_release/dtk23.10/torch-2.1.0a0+git793d2b5.abi0.dtk2310-cp38-cp38-manylinux2014_x86_64.whl
wget https://cancon.hpccube.com:65024/directlink/4/tensorflow/previous_release/dtk22.10/tensorflow-2.9.0+git04231bcc.dtk2210-cp38-cp38-manylinux2014_x86_64.whl
wget https://cancon.hpccube.com:65024/directlink/4/tensorflow/previous_release/dtk22.10/libtensorflow.2.9+git04231bcc_dtk2210-cp38.tar.xz
```

由于pytorch没有基于DTK22.10的版本，这里选择基于高版本DTK 23.10兼容DTK 22.10。鉴于以上的版本要求，选择Python 3.8环境。

下载Deepmd-kit

```bash
git clone https://github.com/deepmodeling/deepmd-kit.git
mv  deepmd-kit deepmd-kit-2.2.10
tar czvf deepmd-kit-2.2.10.tar.gz deepmd-kit-2.2.10
```

配置编译环境

```bash
module purge
module add apps/anaconda3/2023.09
module load compiler/devtoolset/7.3.1
module load compiler/cmake/3.24.1
module load mpi/hpcx/2.7.4/gcc-7.3.1
module load mathlib/fftw/3.3.8/double/gnu
module load mathlib/openblas/gnu/0.3.7
module load mathlib/gsl/2.7/gnu
module load compiler/rocm/dtk/23.10
#source /public/software/compiler/dtk/dtk-23.10/env.sh
```

这里选择mpi/hpcx/2.7.4/gcc-7.3.1，而不是mpi/hpcx/2.12.0/gcc-7.3.1，是因为后者在cmake时找不到MPI_CXX而报错。

创建Conda Python 3.8本地环境 (实质为python-3.8.19)

```bash
conda create -n py3dl python=3.8
source activate py3dl
pip install tensorflow-2.9.0+git04231bcc.dtk2210-cp38-cp38-manylinux2014_x86_64.whl
pip install torch-2.1.0a0+git793d2b5.abi0.dtk2310-cp38-cp38-manylinux2014_x86_64.whl
```

解压libtensorflow并拷贝到site-packages/tensorflow目录（备用）

```
tar xvf libtensorflow.2.9+git04231bcc_dtk2210-cp38.tar.xz
cp dist/* ~/.conda/envs/py3dl/lib/python3.8/site-packages/tensorflow
```

创建使用环境dl.sh

```bash
module purge
module load apps/anaconda3/2023.09
module load compiler/devtoolset/7.3.1
module load compiler/cmake/3.24.1
module load mpi/hpcx/2.7.4/gcc-7.3.1
module load mathlib/fftw/3.3.8/double/gnu
module load mathlib/openblas/gnu/0.3.7
module load mathlib/gsl/2.7/gnu
module load compiler/rocm/dtk/23.10

source activate py3dl
export LD_LIBRARY_PATH=$CONDA_PREFIX/lib:$CONDA_PREFIX/deepmd-2.2.10/lib:$CONDA_PREFIX/lammps-17Aug2024/lib64:$CONDA_PREFIX/lib/python3.8/site-packages/torch/lib:$CONDA_PREFIX/lib/python3.8/site-packages/tensorflow:$CONDA_PREFIX/lib/python3.8/site-packages/tensorflow/python:$LD_LIBRARY_PATH
export PATH=$CONDA_PREFIX/deepmd-2.2.10/bin:$CONDA_PREFIX/lammps-17Aug2024/bin:$PATH
```

注意这里我们使用DTK 23.10环境而不是DTK 22.10环境。

先**[编译DeepMD-kit C++接口](https://docs.deepmodeling.com/projects/deepmd/en/master/install/install-from-source.html#install-deepmd-kit-s-c-interface)**，安装到`$CONDA_PREFIX/deepmd-2.2.10`

```bash
mkdir -p deepmd-kit/source/build
cd deepmd-kit/source/build

cmake -L ../ \
-DDP_VARIANT=rocm \
-DTENSORFLOW_ROOT=$CONDA_PREFIX/lib/python3.8/site-packages/tensorflow \
-DCMAKE_INSTALL_PREFIX=$CONDA_PREFIX/deepmd-2.2.10 \
-DUSE_ROCM_TOOLKIT=TRUE \
-DLAMMPS_SOURCE_ROOT=$HOME/lammps_dev/lammps-17Apr2024 \
-DLAMMPS_VERSION_NUMBER=20240417 \
-DCMAKE_BUILD_TYPE=Release \
-DUSE_TF_PYTHON_LIBS=TRUE

make -j24
make install
make lammps
cp -r USER-DEEPMD $HOME/lammps_dev/lammps-17Apr2024/src/
```

其中使用Conda环境下的ternsorflow，默认自动识别`ROCM_ROOT`，否则手动添加`-DROCM_ROOT=${ROCM_PATH}`。编译之后，USER-DEEPMD是DeepMD在LAMMPS中的扩展包，将其拷贝到LAMMPS软件中。

**编译LAMMPS module**

编译完DeepMD-kit C++接口之后，我们通过cmake以build-in模式[编译LAMMPS的DeepMD-kit模块](https://docs.deepmodeling.com/projects/deepmd/en/master/install/install-lammps.html)。

```
cd ~/lammps_dev/lammps-17Apr2024
echo "include($HOME/lammps_dev/deepmd-kit-2.2.10/source/lmp/builtin.cmake)" >> cmake/CMakeLists.txt
```

将Allegro导入LAMMPS

```
git clone https://github.com/mir-group/pair_allegro.git
cd pair_allegro
./patch_lammps.sh ~/lammps_dev/lammps-17Apr2024
```

修改将kokkos-hip.cmake中`Kokkos_ARCH_VEGA90A`改为`Kokkos_ARCH_VEGA906`

开始编译，

```bash
cd lammps-17Apr2024
mkdir build && cd build
export TORCH_PATH=$CONDA_PREFIX/lib/python3.8/site-packages/torch

cmake -L -C ../cmake/presets/most.cmake -C ../cmake/presets/hip_amd.cmake \
-C ../cmake/presets/kokkos-hip.cmake \
-DTorch_DIR=`python -c 'import torch;print(torch.utils.cmake_prefix_path)'`/Torch \
-DGFLAGS_INCLUDE_DIR=$TORCH_PATH/include \
-DGFLAGS_LIBRARY=$TORCH_PATH/lib/libgflags.so \
-DGLOG_INCLUDE_DIR=$TORCH_PATH/include \
-DGLOG_LIBRARY=$TORCH_PATH/lib/libglog.so \
-DMKL_INCLUDE_DIR=/public/software/compiler/intel/2021.3.0/mkl/include \
-DCMAKE_INSTALL_PREFIX=$CONDA_PREFIX/lammps-17Aug2024 -DBUILD_TOOLS=ON -DBUILD_SHARED_LIBS=ON \
-DPKG_COLVARS=ON -DPKG_PLUMED=ON -DPKG_GPU=ON -DGPU_API=hip -DHIP_ARCH=gfx906 \
-DCMAKE_PREFIX_PATH=$CONDA_PREFIX/deepmd-2.2.10 \
-DFFT=FFTW3 -DFFTW3_LIBRARY=/public/software/mathlib/fftw/3.3.8/double/gnu/lib/libfftw3.so \
-DFFTW3_INCLUDE_DIR=/public/software/mathlib/fftw/3.3.8/double/gnu/include \
-DBUILD_MPI=ON -DLAMMPS_INSTALL_RPATH=ON -DLAMMPS_MACHINE=mpi ../cmake
```

无需设置`-DPKG_USER-DEEPMD=ON`，但需要指定CMAKE_PREFIX_PATH告诉`find_package(DeePMD REQUIRED)`的搜索路径。

以上编译使用Conda环境下的Pytorch，其中gflags和glog使用Pytorch自带的版本。

使用命令行打上编译选项补丁

```bash
sed -i -e "s|/opt/dtk/hip/lib/libamdhip64.so|/public/software/compiler/dtk/dtk-23.10/hip/lib/libamdhip64.so|g" \
    -e "s|/opt/dtk/roctracer/lib/libroctx64.so|/public/software/compiler/dtk/dtk-23.10/roctracer/lib/libroctx64.so|g" \
    CMakeFiles/lammps.dir/build.make CMakeFiles/lmp.dir/build.make
    
sed -i -e "s|-lomp|-L/public/software/compiler/dtk/dtk-23.10/llvm/lib/ -lomp|g" \
    -e "s|-lroctx64|-L/public/software/compiler/dtk/dtk-23.10/roctracer/lib -lroctx64|g" \
    -e "s|/public/software/apps/anaconda3-2023.09/lib/libgflags.so.2.2.2|$HOME/.conda/envs/py3dl/lib/python3.8/site-packages/torch/lib/libgflags.so|g" \
    -e "s|/public/software/apps/anaconda3-2023.09/lib/libglog.so.0.5.0|$HOME/.conda/envs/py3dl/lib/python3.8/site-packages/torch/lib/libglog.so|g" \
    CMakeFiles/lammps.dir/link.txt CMakeFiles/lmp.dir/link.txt
```
再继续编译安装
```
make -j12
make install
```

为了更好地使用LAMMPS，将配置文件`dl.sh`拷贝到py3dl环境中`$CONDA_PREFIX/lammps-17Aug2024/bin/lmp_env_dl.sh`

在py3dl环境中安装计算材料常用的Python软件包环境

```bash
pip install wandb nequip
git clone https://github.com/mir-group/allegro.git
cd allegro
pip install . 
```

安装**DeepMD-kit Python接口**

```
pip install dpdata mpi4py h5py pandas spglib seekpath phonopy
cd deepmd-kit-2.2.10
export DP_VARIANT=rocm
pip install .
```

只有安装过DeepMD-kit Python接口之后，才有dp命令可用。

导出py3dl环境软件配置列表

```
conda env export > ~/.conda/envs/py3dl/etc/py3dl_env.yaml
```

打包发布

```bash
$ du -hs ~/.conda/envs/py3dl/
8.6G   /public/home/xushun/.conda/envs/py3dl/
$ cd ~/.conda/envs 
$ tar czvf ~/py3dl-env.tar.gz py3dl
```

其他用户只要解压 py3dl-env.tar.gz 到本地~/.conda/envs/py3dl目录即可使用。

py3dl环境说明：

- LAMMPS安装路径 `$CONDA_PREFIX/lammps-17Aug2024`
- LAMMPS运行配置文件 `$CONDA_PREFIX/lammps-17Aug2024/bin/lmp_env_dl.sh`
- LAMMPS测试实例目录`$CONDA_PREFIX/lammps-17Aug2024/examples` ，包含Allegro和[DeepMD](https://tutorials.deepmodeling.com/en/latest/Tutorials/DeePMD-kit/learnDoc/run5minutes.html)实例
- DeepMD安装路径 `$CONDA_PREFIX/deepmd-2.2.10`
- Nequip和Allegro软件工具已通过py3dl环境下pip安装
- py3dl环境软件配置列表 `$CONDA_PREFIX/etc/py3dl_env.yaml`
- 构建py3dl环境的安装包和源代码 `$CONDA_PREFIX/packages`



**实例运行**

LAMMPS的Torch+TensorFlow版本

- 公开目录环境为`/public/software/apps/lammps-dl/py3dl/lammps-17Aug2024/bin/lmp_env_dl.sh`

- 本地安装环境为`$HOME/.conda/envs/py3dl/lammps-17Aug2024/bin/lmp_env_dl.sh`

如果没有本地安装（成功），以下实例测试可使用公共目录下安装的版本，更改`lmp_env_d.sh`的路径即可使用。

首先检测ternsorflow是否可用，之后检测LAMMPS是否支持Deepmd和Allegro机器学习势，测试脚本test_dl.slurm如

```bash
#!/bin/bash
#SBATCH -J dl
#SBATCH -p normal
#SBATCH --ntasks-per-node=2
#SBATCH --cpus-per-task=4
#SBATCH --gres=dcu:2
#SBATCH -o %x-%j.log

#env|grep SLURM
echo "HIP_VISIBLE_DEVICES = $HIP_VISIBLE_DEVICES"

source $HOME/.conda/envs/py3dl/lammps-17Aug2024/bin/lmp_env_dl.sh
which python
python -c "import tensorflow as tf;print(tf.reduce_sum(tf.random.normal([1000, 1000])))"
python -c "import torch;print(torch.compiled_with_cxx11_abi())"
mpirun -n ${SLURM_NTASKS} lmp_mpi -nocite -h |grep -E "deepmd|allegro"
```

运行命令

```
conda deactivate
sbatch test_dl.slurm
```

获得结果如

```
HIP_VISIBLE_DEVICES = 0,1
/public/home/xushun/.conda/envs/py3dl/bin/python
2024-07-08 00:08:40.736489: I tensorflow/core/platform/cpu_feature_guard.cc:193] This TensorFlow binary is optimized with oneAPI Deep Neural Network Library (oneDNN) to use the following CPU instructions in performance-critical operations:  SSE3 SSE4.1 SSE4.2 AVX AVX2 FMA
To enable them in other operations, rebuild TensorFlow with the appropriate compiler flags.
2024-07-08 00:08:40.739553: I tensorflow/core/common_runtime/gpu/gpu_device.cc:1532] Created device /job:localhost/replica:0/task:0/device:GPU:0 with 15601 MB memory:  -> device: 0, name: Device 66a1, pci bus id: 0000:04:00.0
2024-07-08 00:08:40.761179: I tensorflow/core/common_runtime/gpu/gpu_device.cc:1532] Created device /job:localhost/replica:0/task:0/device:GPU:1 with 15601 MB memory:  -> device: 1, name: Device 66a1, pci bus id: 0000:26:00.0
tf.Tensor(507.97403, shape=(), dtype=float32)
False
airebo/morse/omp                airebo/omp      allegro         allegro3232
allegro6464     allegro3264     allegro6432     allegro/kk      allegro3232/kk
allegro6464/kk  allegro3264/kk  allegro6432/kk  amoeba          amoeba/gpu
coul/wolf/cs    coul/wolf/kk    coul/wolf/omp   deepmd          reax
ackland/atom    adf             aggregate/atom  allegro         allegro/atom
```

也可用使用命令行简单测试

```bash
source $HOME/.conda/envs/py3dl/lammps-17Aug2024/bin/lmp_env_dl.sh
srun -p normal -n 1 --gres=dcu:1 mpirun -n 1 lmp_mpi -h|grep -E "deepmd|allegro"
```



# 四、实例测试

## 配置说明

为了使用方便，可以定义LAMMPS安装目录变量，如

```
export LMP_DIR=/public/home/xushun/local/lammps-2Aug2023
```

则可执行文件路径为`$LMP_DIR/bin/lmp_hip`。

本项目提供已经编译好的二进制文件 [lammps-2Aug2023.tar.gz](https://gitlab.com/orise/lammps-hip/-/blob/master/install/lammps-2Aug2023.tar.gz) 点击下载，加压后就可以使用，其中[install/submit.sh](https://gitlab.com/orise/lammps-hip/-/blob/master/install/submit.sh)是调用的模板，可参照自行修改。

在东方超算系统中，可使用如下命令查看lmp_hip编译时的配置信息：

```
srun -p normal -n 1 --gres=dcu:1 mpirun -n 1 $LMP_DIR/bin/lmp_hip -h
```

可以获得LAMMPS的版本号，还会输出GPU package编译所用加速方式和LAMMPS整数字节数：

```
Accelerator configuration:

GPU package API: HIP
GPU package precision: mixed
OPENMP package API: OpenMP
OPENMP package precision: double

Compatible GPU present: yes

Active compile time flags:

-DLAMMPS_GZIP
-DLAMMPS_PNG
-DLAMMPS_JPEG
-DLAMMPS_SMALLBIG
sizeof(smallint): 32-bit
sizeof(imageint): 32-bit
sizeof(tagint):   32-bit
sizeof(bigint):   64-bit
```

还会输出所编译安装的pacakge列表：

```
Installed packages:

AMOEBA ASPHERE BOCS BODY BPM BROWNIAN CG-DNA CG-SPICA CLASS2 COLLOID COLVARS
COMPRESS CORESHELL DIELECTRIC DIFFRACTION DIPOLE DPD-BASIC DPD-MESO DPD-REACT
DPD-SMOOTH DRUDE EFF ELECTRODE EXTRA-COMPUTE EXTRA-DUMP EXTRA-FIX
EXTRA-MOLECULE EXTRA-PAIR FEP GPU GRANULAR INTERLAYER KSPACE LEPTON MACHDYN
MANYBODY MC MEAM MESONT MISC ML-IAP ML-POD ML-SNAP MOFFF MOLECULE OPENMP OPT
ORIENT PERI PHONON PLUGIN PLUMED POEMS QEQ REACTION REAXFF REPLICA RIGID SHOCK
SPH SPIN SRD TALLY UEF VORONOI YAFF
```

以及输出编译支持的LAMMPS作用势类型、积分类型、最小化类型和对相互作用类型：

```
List of individual style options included in this LAMMPS executable

* Atom styles:

amoeba          angle           atomic          body            bond
bpm/sphere      charge          dielectric      dipole          dpd
edpd            electron        ellipsoid       full            hybrid
line            mdpd            molecular       oxdna           peri
smd             sph             sphere          spin            tdpd
template        tri

* Integrate styles:

respa           respa/omp       verlet          verlet/split

* Minimize styles:

cg              fire/old        fire            hftn            quickmin
sd              spin            spin/cg         spin/lbfgs

* Pair styles:

adp             adp/omp         agni            agni/omp        airebo
airebo/morse    airebo/morse/omp                airebo/omp      amoeba
amoeba/gpu      atm             beck            beck/gpu        beck/omp
body/nparticle  body/rounded/polygon            body/rounded/polyhedron
bop             born            born/coul/dsf   born/coul/dsf/cs
born/coul/long  born/coul/long/cs               born/coul/long/cs/gpu
born/coul/long/gpu              born/coul/long/omp              born/coul/msm
#...此处省略多行
```

需要注意对于某些对相互作用势（Pair style）有`omp`和`gpu`后缀，分别表示使用OpenMP和GPU加速计算的作用势，如`beck/gpu`表示具有GPU加速的beck对相互作用势。有些作用势名称后不带gpu，表明该作用势不支持GPU加速计算。因此使用GPU加速计算之前，需求确认当前LAMMPS编译版本中是否有相应作用势的GPU加速的版本。

**使用GPU计算**

LAMMPS GPU加速是通过gpu package来实现的。它的设计是为了利用常见的GPU硬件配置，可以将CPU处理器核与GPU卡耦合在一起，在LAMMPS作业的单个进程中，其中计算密集型的任务通过offload（卸载）方式提交到GPU设备上运行，并将计算结果返回给CPU进程。gpu package主要实现如下特征

- 原子的数据（例如坐标、力）在CPU和GPU之间每隔一段时间来回移动
- 邻居列表可以建立在CPU或GPU上
- PPPM的电荷分配和力插补部分可以在GPU上运行，FFT部分在CPU上运行，它需要处理器之间的MPI通信
- 不同类型的力计算（Pair/键/角/二面角/非正当二面角）可以分别在GPU和CPU上同时执行，对于force计算，可以设置CPU和GPU分工计算的任务比例
- 允许GPU计算以单精度或双精度执行，或以混合模式精度执行，其中成对力以单精度计算，但累积成双精度力向量。

GPU包支持多个MPI任务，调用同一块GPU加速，但不支持单个MPI任务调用多块GPU卡。

要启动GPU包，可以在input文件中显示添加gpu后缀，如pair_style lj/cut/gpu 2.5，但可以在命令行通过`-sf gpu`选项开启GPU计算，并在各个style名后面自动加上gpu后缀，使得input文件中的style自动加载gpu版本，这种使用方式很方便，不需要修改input文件。

另外还可以通过命令行选项`-pk gpu n`来设置每个节点可用GPU数，默认为1，最大为单节上实际配置的物理GPU卡数，如`n=0`表示自动选择可用GPU卡数。关于GPU package的介绍详见 https://lammps.sandia.gov/doc/Speed_gpu.html。

在东方超算系统的Slurm提交作业时，需要使用`--gres=dcu:n`来申请DCU卡数，否则LAMMPS主程序找不到DCU卡。DCU卡编号从0开始。

```bash
#SBATCH -J lj
#SBATCH -p normal

#SBATCH -N 1
#SBATCH --ntasks-per-node=4
#SBATCH --exclusive
#SBATCH --gres=dcu:4

mpirun -n ${SLURM_NTASKS} lmp_hip -nocite -sf gpu -pk gpu 4 -in in.lj -log lj-hip.log 
```

以上参数申请1个节点（`-N 1`），节点上的进程数为4(`--ntasks-per-node=4`)，节点上分配4块DCU卡(`--gres=dcu:4`)。在lmp_hip命令行中针对`in.lj`文件，启动了gpu后缀，并使用4块DCU卡。以上设置为单个MPI进程搭配用一块DCU卡加速。当`--ntasks-per-node=8`时，则每两个MPI进程搭配用一块DCU卡。



## 基准测试

本文档提供一个附件[bench](https://gitlab.com/orise/lammps-hip/-/tree/master/bench)，提供DPD、LJ、Martini DPPC、Polymer和Water等5种测试体系。针对东方超算系统Slurm作业调度系统，为每个体系提供了作业提交脚本submit.sh，内含纯CPU和DCU加速两种运行模式的配置选项，并且通过Makefile文件提供了统一的操作接口，相关命令操作如下

```bash
make conf #修改Makefile之后生效。
make clean #清理之前的log文件
make submit #重新提交任务
make cancel #取消slurm所有作业
make log #性能log输出
make logn #数值型log输出
make tar #打包发布 
```

首先修改Makefile文件，为SLRUM_PARTITION、LMP_PATH、OMP_NUM_THREADS等变量设置正确的值，这几个值影响bench目录提供的所有体系：

```makefile
#change the configuration. and then make conf to apply it
PKGNAME=lmp_bench
SLRUM_PARTITION=normal
LMP_PATH=/public/home/xushun/local/lammps-2Aug2023/bin/lmp_hip
OMP_NUM_THREADS=1
```

在bench目录使用以下命令使得配置参数生效，即自动更新每个实例目录中的submit.sh文件

```
make conf
```
通过submit这个Make目标，调用sbatch提交每个实例的submit.sh
```
make submit
```

使用make命令提取log文件的性能参数

```
#cd bench
make log
```

所有任务运行完毕后，通过log日志提取性能数据，以列表方式给出各个实例的性能结果如：

| Case | tau/day |       timesteps/s      | procs | atoms   |
| ---- | ------------------ | ----------- | ----- | ------- |
| LJ-DCU | 30767.238 | 71.220 | 512 | 131072000 |
| LJ-CPU | 1467.847 | 3.398 | 512 | 131072000 |
| DPD-DCU | 1007080.278 | 582.801 | 128   | 2048000 |
| DPD-CPU | 84170.081 | 48.710 | 128 | 2048000 |


| Case | ns/day |       timesteps/s      | procs | atoms   |
| ---- | ------------------ | ----------- | ----- | ------- |
| Polymer-DCU | 28.546 | 330.395 | 256 | 75232 |
| Polymer-CPU | 13.953 | 161.490 | 256 | 75232 |
| Water-DCU | 89.845 | 519.936 | 32  | 12288 |
| Water-CPU | 28.358 | 164.110 | 32 | 12288 |
| Martini DPPC-DCU | 13291.461 | 5127.879 | 256 | 9600 |
| Martini DPPC-CPU | 13396.604 | 5168.443 | 256 | 9600 |

以上结果每次运行可能有小的差异。对于LJ、DPD、Polymer和Water等体系DCU加速显著；但是对于Martini DPDC体系即使开启了GPU模式，而input文件中采用的Pair style `lj/gromacs/coul/gromacs`并没有GPU加速的版本可调用，程序仍然以CPU模式运行，因此前后两种运行模型，性能水平差别不大。



## 参考链接

- LAMMPS各种加速计算包 https://docs.lammps.org/Speed_packages.html
- Slurm资源管理与作业调度系统安装配置 http://hmli.ustc.edu.cn/doc/linux/slurm-install/index.html
- SLURM 使用参考 http://faculty.bicmr.pku.edu.cn/~wenzw/pages/slurm.html
