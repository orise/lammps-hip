#!/bin/bash
#SBATCH -J lugano
#SBATCH -p normal
#SBATCH -N 2 
#SBATCH --ntasks-per-node=4
#SBATCH --cpus-per-task=1
#SBATCH --exclusive
#SBATCH --gres=dcu:4
#SBATCH -o %x-%j.log 

#env|grep SLURM

export LMP_PATH=/public/home/xushun/local/lammps-2Aug2023/bin/lmp_hip
export LD_LIBRARY_PATH=/opt/rocm/hsa/lib:$LD_LIBRARY_PATH
export NAME_PREFIX=${SLURM_JOB_NAME}-${SLURM_JOBID}-${SLURM_NTASKS}n${SLURM_NNODES}N
export OMP_NUM_THREADS=1

srun hostname |sort|awk 'NF==1{arr[$1]++} END{for (i in arr){printf "%s:%d\n",i,arr[i]}}'


#CPU mode
date
mpirun -n ${SLURM_NTASKS} ${LMP_PATH} -nocite -in lammps-equilibration.in -log ${NAME_PREFIX}-equ.log 
#mpirun -n ${SLURM_NTASKS} ${LMP_PATH} -nocite -in lammps-production.in -log ${NAME_PREFIX}-prod.log 


#sbatch this.sh
