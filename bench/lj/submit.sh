#!/bin/bash
#SBATCH -J lj
#SBATCH -p normal
#SBATCH -N 128 
#SBATCH --ntasks-per-node=4
#SBATCH --cpus-per-task=1
#SBATCH --exclusive
#SBATCH --gres=dcu:4
#SBATCH -o %x-%j.log 

#env|grep SLURM

export LMP_PATH=/public/home/xushun/local/lammps-2Aug2023/bin/lmp_mpi
export NAME_PREFIX=${SLURM_JOB_NAME}-${SLURM_JOBID}-${SLURM_NTASKS}n${SLURM_NNODES}N
export OMP_NUM_THREADS=1

srun hostname |sort|awk 'NF==1{arr[$1]++} END{for (i in arr){printf "%s:%d\n",i,arr[i]}}'

#GPU mode
date
mpirun -n ${SLURM_NTASKS} ${LMP_PATH} -nocite -var x 4 -var y 4 -var z 4 -sf gpu -pk gpu 4 -in in.lj -log ${NAME_PREFIX}-hip.log 

date
mpirun -n ${SLURM_NTASKS} ${LMP_PATH} -nocite -var x 4 -var y 4 -var z 4 -k on g 4 -sf kk -in in.lj -log ${NAME_PREFIX}-hip.log 

#CPU mode
date
mpirun -n ${SLURM_NTASKS} ${LMP_PATH} -nocite -var x 4 -var y 4 -var z 4 -in in.lj -log ${NAME_PREFIX}-cpu.log 


#sbatch this.sh
