#!/bin/bash
#SBATCH -J dpd
#SBATCH -p normal
#SBATCH -N 4
#SBATCH --ntasks-per-node=4
#SBATCH --cpus-per-task=1
#SBATCH --exclusive
#SBATCH --gres=dcu:4
#SBATCH -o %x-%j.log 

#env|grep SLURM

export LMP_PATH=lmp_mpi #/public/home/xushun/local/lammps-2Aug2023/bin/lmp_hip
export NAME_PREFIX=${SLURM_JOB_NAME}-${SLURM_JOBID}-${SLURM_NTASKS}n${SLURM_NNODES}N
export OMP_NUM_THREADS=1

srun hostname |sort|awk 'NF==1{arr[$1]++} END{for (i in arr){printf "%s:%d\n",i,arr[i]}}'

#GPU mode
date
mpirun -n ${SLURM_NTASKS} ${LMP_PATH} -nocite -var x 1 -var y 1 -var z 1 -sf gpu -pk gpu 4 -in dpd.in -log ${NAME_PREFIX}-gpu.log

date
mpirun -n ${SLURM_NTASKS} ${LMP_PATH} -nocite -var x 1 -var y 1 -var z 1 -k on g 4 -sf kk -pk kokkos newton on neigh half gpu/aware off -in dpd.in -log ${NAME_PREFIX}-kk.log
#CPU mode
date
mpirun -n ${SLURM_NTASKS} ${LMP_PATH} -nocite -var x 1 -var y 1 -var z 1 -in dpd.in -log ${NAME_PREFIX}-cpu.log 

#sbatch this.sh
